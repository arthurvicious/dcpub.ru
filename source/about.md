---
layout: page
title: About
permalink: /about/
---

Мы сообщества DEFCON, которые хотим поддерживать социализацию в это непростое время. Да и потом тоже хотим, а то что это получается? Одна группа людей пошла в паб, а другие в других городах не могут с ними выпить? Несправедлиииво! Вот эту проблемы мы и хотим решить. <a href="https://dc20e6.ru/whoami/">А что такое DC/DEFCON/ДиСи?</a>

Конечно же наш паб открыт не только для DEFCON сообществ, мы рады всем :) [Напишите нам откуда вы и на этой странице скоро появится карта со списком сообществ/чатиков и сайтов, откуда к нам гости приходили %)](https://forms.gle/An9aQH6AZhzgTUfr8)

---

#### Сообщества DEFCON-ru (DC-ru)

<ul>
  <li><a href="https://defcon-russia.ru/">DC7812 - Санкт Петербург</a></li>
  <li><a href="http://defcon.su/">DC7499 - Москва</a></li>
  <li><a href="https://dc20e6.ru/">DC8422(DC20e6) - Ульяновск</a></li>
  <li><a href="http://defcon-nn.ru/">DC7831 - Нижний Новгород</a></li>
  <li><a href="https://dc7495.org/">DC7495 - Зеленоград</a></li>
  <li><a href="https://t.me/defcon7343">DC7343 - Екатеринбург</a></li>
  <li><a href="https://t.me/dc7342">DC7342 - Пермь</a></li>
  <li><a href="http://defcon58.ru/">DC78412 - Пенза</a></li>
  <li><a href="http://dc8800.ru/">DC8800 – У них нет города, они просто делают свое дело.</a></li>
  <li>Полный список сообществ доступен по ссылке: <a href="https://defcongroups.org/dcpages-int.html">https://defcongroups.org/dcpages-int.html</a></li>
</ul>
